package cl.ubb.dao;

import org.springframework.data.repository.CrudRepository;
import java.util.ArrayList;
import cl.ubb.model.Cliente;


public interface ClienteDao extends CrudRepository<Cliente, Long>  {

	public ArrayList<Cliente> findClientes(String rut, String nombre, long numeroTelefono);
	
	public Cliente findByName(String name);
	
	public Cliente findByRut(String rut);
	
	public Cliente findByEmail(String email);
	
	public Cliente findByNro(int phone);
	
	public ArrayList<Cliente> findByCategoria(String categoria);
}
