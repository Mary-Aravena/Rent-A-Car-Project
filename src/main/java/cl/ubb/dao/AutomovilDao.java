package cl.ubb.dao;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.Automovil;

public interface AutomovilDao extends CrudRepository<Automovil,Long> {
	
	public ArrayList<Automovil> findByCategoria(String marca, String modelo, String tipoTransmicion);

}
