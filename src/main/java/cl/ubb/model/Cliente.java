package cl.ubb.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Cliente{
	
	@Id
	@GeneratedValue
	private long id;
	private String rut;
	private String nombre;
	private long celular;
	private String email;
	private String categoria; //Empresa, particular
	
	public Cliente(){
		
	}

	public long getId(){
		return id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	public String getRut() {
		return rut;
	}
	
	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public long getCelular() {
		return celular;
	}
	
	public void setCelular(long celular) {
		this.celular = celular;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setCategoria(String categoria){
		this.categoria = categoria;
	}
	
	public String getCategoria() {
		return categoria;
	}
	
}