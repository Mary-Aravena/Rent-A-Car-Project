package cl.ubb.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Automovil{
	
	@Id
	@GeneratedValue
	private long id;
	private String marca;
	private String modelo;
	private String categoria; //Lujo, transporte, citycar
	private String tipoTransmicion;
	
	public Automovil(){
		
	}

	public long getId(){
		return id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	public String getMarca() {
		return marca;
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}
	
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public String getCategoria() {
		return categoria;
	}
	
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getTipoTransmicion() {
		return tipoTransmicion;
	}
	
	public void setTipoTransmicion(String tipoTransmicion) {
		this.tipoTransmicion = tipoTransmicion;
	}
}