package cl.ubb.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Reserva{
	
	@Id
	@GeneratedValue
	private long id;
	private String fechaInicio;
	private String fechaFin;
	private long idAutomovil;
	private long idCliente;
	
	public Reserva(){
		
	}

	public long getId(){
		return id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	public long getIdAtomovil(){
		return idAutomovil;
	}
	
	public void setIdAutomovil(long idAutomovil){
		this.idAutomovil = idAutomovil;
	}

	public long getIdCliente(){
		return idCliente;
	}
	
	public void setIdCliente(long idCliente){
		this.idCliente = idCliente;
	}
}