package cl.ubb.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.ubb.dao.ReservaDao;
import cl.ubb.model.Cliente;
import cl.ubb.model.Reserva;
import cl.ubb.model.Automovil;

@Service
public class ReservaService {
	
	@Autowired
	private ReservaDao reservaDao;
	
	public ArrayList<Reserva> reservarAuto(long id, String fechaInicio, String fechaFin, String categoriaAuto) {
		ArrayList<Reserva> miReserva = new ArrayList<Reserva>();
		Reserva reserva = new Reserva();
		Cliente cliente = new Cliente();
		Automovil automovil = new Automovil();
			
		long idCliente = cliente.getId();
		long idAuto = automovil.getId();
		String fechaInit = reserva.getFechaInicio();
		String fechaEnd = reserva.getFechaFin();
			
		miReserva= (ArrayList<Reserva>) reservaDao.findByReserva(idCliente, idAuto, fechaInit, fechaEnd);
		return miReserva;
	}

	public ArrayList<Reserva> listarReservasDeCliente(long id, String fechainicio) {
		ArrayList<Reserva> misReservas = new ArrayList<Reserva>();
		Reserva reserva = new Reserva();
		
		long idAuto = reserva.getIdAtomovil();
		String fechaInit = reserva.getFechaInicio();
		String fechaEnd = reserva.getFechaFin();
		
		misReservas = (ArrayList<Reserva>) reservaDao.findByReservaCliente(idAuto, fechaInit, fechaEnd);
		return misReservas;
	}
}
