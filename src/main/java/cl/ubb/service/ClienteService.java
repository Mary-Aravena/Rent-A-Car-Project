package cl.ubb.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.ClienteDao;
import cl.ubb.model.Cliente;

public class ClienteService {

		@Autowired
		private ClienteDao clienteDao;
		
		public Cliente registroCliente(Cliente cliente){
			
			return clienteDao.save(cliente);
		}
		
		public ArrayList<Cliente> obtenerTodos() {
			ArrayList<Cliente> misClientes = new ArrayList<Cliente>();
			Cliente cliente = new Cliente();
			
			String rut=cliente.getRut();
			String nombre=cliente.getNombre();
			long numeroTelefono=cliente.getCelular();
			
			misClientes= (ArrayList<Cliente>) clienteDao.findClientes(rut, nombre,numeroTelefono);
			return misClientes;
		}

		public String obtenerPorRut(String rut) {
			Cliente cliente = new Cliente();
			cliente = clienteDao.findByRut(rut);
			if(cliente == null){
				return "no";
			}
			else{
				return "si";
			}
		}
		
		public ArrayList<Cliente> obtenerPorCategoria(String categoria) {
			ArrayList<Cliente> misClientes = new ArrayList<Cliente>();			
			misClientes= (ArrayList<Cliente>) clienteDao.findByCategoria(categoria);
			return misClientes;
		}
		
		
}
