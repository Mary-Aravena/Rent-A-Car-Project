package cl.ubb.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.ubb.dao.AutomovilDao;
import cl.ubb.model.Automovil;

@Service
public class AutomovilService {
	
	@Autowired
	private AutomovilDao automovilDao;
	
	public ArrayList<Automovil> obtenerAutomovilesPorCategoria(String categoria) {
		ArrayList<Automovil> misAutomoviles = new ArrayList<Automovil>();
		Automovil automovil = new Automovil();
		
		String marca=automovil.getMarca();
		String modelo=automovil.getModelo();
		String tipoTransmicion=automovil.getTipoTransmicion();
		
		misAutomoviles= (ArrayList<Automovil>) automovilDao.findByCategoria(marca, modelo, tipoTransmicion);
		return misAutomoviles;
	}
}
