package cl.ubb;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import cl.ubb.dao.AutomovilDao;
import cl.ubb.model.Automovil;
import cl.ubb.model.Cliente;
import cl.ubb.service.AutomovilService;

public class automovilServiceTest {
	
	@Mock
	private AutomovilDao automovilDao;
		
	@InjectMocks
	private AutomovilService automovilService;
	
	@Test 
	public void ListarAutomovilesPorTipo() throws Exception{
		//arrange
		ArrayList<Automovil> misAutomoviles = new ArrayList<Automovil>();
		ArrayList<Automovil> resultadoAutomoviles = new ArrayList<Automovil>();
		
		//act
		when(automovilDao.findAll()).thenReturn(misAutomoviles);
		resultadoAutomoviles = automovilService.obtenerAutomovilesPorCategoria("Lujo");
				
		//assert
		Assert.assertNotNull(resultadoAutomoviles);
		Assert.assertEquals(resultadoAutomoviles, misAutomoviles);
	}
	
	

}
