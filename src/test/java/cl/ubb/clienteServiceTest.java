package cl.ubb;
import  org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.ClienteDao;
import cl.ubb.model.Cliente;
import cl.ubb.service.ClienteService;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.junit.Assert.*;

import org.junit.Test;

@RunWith(MockitoJUnitRunner.class)
public class clienteServiceTest {

	//clase a la cual se realizará mock
		@Mock
		private ClienteDao clienteDao;
		
		@InjectMocks
		private  ClienteService clienteService; 
		
		
	@Test
	public void RegistrarCliente() {
		//arrange
		Cliente cliente = new Cliente();
		Cliente clienteCreado= new Cliente();
		
		cliente.setRut("12.345.678-9");
		cliente.setNombre("Fulanito");
		cliente.setCelular(912345678);
		cliente.setEmail("fulanito@gmail.com");
		cliente.setCategoria("particular");
		
		//act
		when(clienteDao.save(cliente)).thenReturn(cliente);
		clienteCreado= clienteService.registroCliente(cliente);
		
		//assert
		Assert.assertNotNull(clienteCreado);
		Assert.assertEquals(clienteCreado, cliente);
	}
	
	@Test 
	public void ListarTodosLosClientes() throws Exception{
		//arrange
		ArrayList<Cliente> misClientes = new ArrayList<Cliente>();
		ArrayList<Cliente> resultadoClientes = new ArrayList<Cliente>();
		
		//act
		when(clienteDao.findAll()).thenReturn(misClientes);
		resultadoClientes = clienteService.obtenerTodos();
				
		//assert
		Assert.assertNotNull(resultadoClientes);
		Assert.assertEquals(resultadoClientes, misClientes);
	}
	
	@Test
	public void IndicarSiClienteEstaRegistrado(){
		//arrange
		Cliente cliente = new Cliente();
		String clienteRegistro;
		
		//act
		when(clienteDao.findByRut(anyString())).thenReturn(cliente);
		clienteRegistro = clienteService.obtenerPorRut(cliente.getRut());
		
		//assert
		Assert.assertNotNull(clienteRegistro);
		Assert.assertEquals("si", clienteRegistro);
	}	
	
	@Test 
	public void ListarCategoriaClientes() throws Exception{
		//arrange
		ArrayList<Cliente> misClientes = new ArrayList<Cliente>();
		ArrayList<Cliente> resultadoClientes = new ArrayList<Cliente>();
		
		//act
		when(clienteDao.findByCategoria("particular")).thenReturn(misClientes);
		resultadoClientes = clienteService.obtenerPorCategoria("particular");
				
		//assert
		Assert.assertNotNull(resultadoClientes);
		Assert.assertEquals(resultadoClientes, misClientes);
	}
	

}
