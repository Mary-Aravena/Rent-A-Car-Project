package cl.ubb;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import cl.ubb.dao.AutomovilDao;
import cl.ubb.dao.ClienteDao;
import cl.ubb.dao.ReservaDao;
import cl.ubb.model.Automovil;
import cl.ubb.model.Cliente;
import cl.ubb.model.Reserva;
import cl.ubb.service.AutomovilService;
import cl.ubb.service.ClienteService;
import cl.ubb.service.ReservaService;

public class reservaServiceTest {
	
	@Mock
	private AutomovilDao automovilDao;
	private ReservaDao reservaDao;
		
	@InjectMocks
	private AutomovilService automovilService;
	private ReservaService reservaService;
	
	@Test
	public void ReservarAuto(){
		//arrange
		Reserva reserva = new Reserva();
		ArrayList<Reserva> reservaRealizada = new ArrayList<Reserva>();
		Automovil automovil = new Automovil();
		Cliente cliente = new Cliente();
		
		cliente.setId(1234567);
		long id = cliente.getId();
		String fechainicio =  reserva.getFechaInicio();
		String fechafin = reserva.getFechaFin();
		automovil.setCategoria("Citycar");
		String categoria = automovil.getCategoria();
		//act
		when(reservaDao.save(reserva)).thenReturn(reserva);
		reservaRealizada = reservaService.reservarAuto(id, fechainicio, fechafin, categoria);
		
		//assert
		Assert.assertNotNull(reservaRealizada);
		Assert.assertEquals(reserva, reservaRealizada);
		verify(reservaDao).save(reserva);
	}
	
	@Test
	public void ListarReservasDeClienteDesdeUnaFecha(){
		//arrange
		Reserva reserva = new Reserva();
		ArrayList<Reserva> reservaRealizada = new ArrayList<Reserva>();
		Automovil automovil = new Automovil();
		Cliente cliente = new Cliente();
		
		ArrayList<Reserva> reservas = new ArrayList<Reserva>();
		ArrayList<Reserva> misReservas = new ArrayList<Reserva>();
		long id = cliente.getId();
		String fechainicio =  reserva.getFechaInicio();
		
		//act
		when(reservaDao.findAll()).thenReturn(reservas);
		misReservas = reservaService.listarReservasDeCliente(id, fechainicio);
		
		//assert
		Assert.assertNotNull(misReservas);
		Assert.assertEquals(reservas, misReservas);
	}

}
